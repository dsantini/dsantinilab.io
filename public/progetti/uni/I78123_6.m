clear;

% Configurazione
xMin=-10
xMax=10
yMin=-7
yMax=7
K=1000 % Numero totale armoniche
V=8 % Numero armoniche visibili

% Codice
sum=0;
clf
x=xMin:.01:xMax;
hold on
for k=1:1:K,
    sum = sum+(6*(-1)^k-1)/k*sin(k*x*pi/5);
    f=-2/pi*sum;
    if (k < V)
      plot(x,f,'DisplayName',num2str(k))
    elseif (k == K)
      plot(x,f,'LineWidth',2,'DisplayName',num2str(K))
    end
end
hold off
legend('show')
title(mfilename)
xlabel('x')
ylabel('F(x)')
grid on
axis([xMin,xMax,yMin,yMax])

% Stampa il grafico
print([mfilename,'.png'],'-dpng','-r300')